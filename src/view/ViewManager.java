package view;

import java.time.LocalTime;
import java.util.Date;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.concurrent.Task;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class ViewManager {

	// obiekty tworzenia okna Stage -ramka ; Scene -scena wewnetrzna; AnchorPane
	// odpowiada za elementy w wew scenie
	public static Stage mainStage;
	public static AnchorPane mainPane;
	private Scene mainScene;

	// Stale do obiektow
	private final static int MAIN_SCENE_WIDTH = 800;
	private final static int MAIN_SCENE_HIGHT = 500;
	private final static String STAGE_TITLE = "Ksiazka Kucharska by Michal Ziolecki";
	private final static String FONT = "Arial";

	AnimationTimer menuTimer;

	// obiekty wlasne GUI
	ButtonAndLabel buttonAndLabel = new ButtonAndLabel();

	// metoda do przypisywania elementow scenie wewnetrznej i ramce
	public ViewManager() // konstruktor
	{
		mainStage = new Stage();
		mainPane = new AnchorPane();
		mainScene = new Scene(mainPane, MAIN_SCENE_WIDTH, MAIN_SCENE_HIGHT); // tworzenie scene z wymianami i wstawiam w
																				// niej elementy typu pane
		//mainScene.setUserAgentStylesheet(Application.STYLESHEET_CASPIAN);
		//mainScene.setFill(Paint.valueOf("blue"));
		mainStage.setScene(mainScene); // przekazuje wewnetrzna scene do ramki
		mainStage.setTitle(STAGE_TITLE); // ustawiam tytul
		mainStage.setResizable(false); // ustawiam scene na sztywno - nie mozna zmieniac rozmiaru okna
		createMainContent();
		createClock();
	}

	public Stage getMainStage() {
		return mainStage; // przekazuje stage do maina a tam do obiektu javafx odpowiedzialnego za
							// wyswietlenie
	}

	private void createMainContent() // metoda zbierajaca metody do tworzenia elementow
	{
		
		mainPane.getChildren().add(buttonAndLabel.getLabel());
		mainPane.getChildren().add(buttonAndLabel.getAddProductButton()); // przekazuje -/+ inicjalizuje je na ekranie
		mainPane.getChildren().add(buttonAndLabel.getAddRecipeButton());
		mainPane.getChildren().add(buttonAndLabel.getShowProductButton());
		mainPane.getChildren().add(buttonAndLabel.getShowRecipesButton());

	}

	private void createClock() {
		Label timeLabel = new Label();
		timeLabel.setFont(new Font("VERDENA", 17.0));
		timeLabel.setLayoutX(700);
		timeLabel.setLayoutY(20);
		mainPane.getChildren().add(timeLabel);
		final Task task = new Task() {
			@Override
			protected Object call() throws Exception {

				menuTimer = new AnimationTimer() {
					@Override
					public void handle(long arg0) {
						Date date = new Date();
						int hours = date.getHours();
						int minutes = date.getMinutes();
						int seconds = date.getSeconds();
						
						String hoursS = null;
						String minutesS = null;
						String secondsS = null;
						if(hours < 10) {
							hoursS = "0" + hours;
						} else {
							hoursS = "" + hours;
						}
						if(minutes < 10) {
							minutesS = "0" + minutes;
						} else {
							minutesS = "" + minutes;
						}
						if(seconds < 10) {
							secondsS = "0" + seconds;
						} else {
							secondsS = "" + seconds;
						}
						timeLabel.setText(hoursS + ":" + minutesS + ":" + secondsS);
					}
				};
				menuTimer.start();
				return null;
			}
		};

		Thread thread = new Thread(task);
		thread.start();
		
	}

}
