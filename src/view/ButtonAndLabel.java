package view;

import javax.swing.text.StyledEditorKit.BoldAction;

import application.MenuButton;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

public class ButtonAndLabel {
	
	//obiekty odpowiadajace przyciskom
	MenuButton addProductButton;
	MenuButton addRecipeButton;
	MenuButton showProductButton;
	MenuButton showRecipesButton;
			
	//wyswietlanie etykiet - reakcja na przycisk
	Label mainLabel;
	Label productNameLabel;
	Label productKcalLebel;
	
	
	//Obiekty wlasne
		
	private final static String FONT = "Arial";
	
	public ButtonAndLabel() 
	{
		mainLabel= new Label("Witam w ksiazke kucharskiej!!");
		//inicjalizacja przyciskow
		addProductButton = new MenuButton("Dodaj produkt");
		addRecipeButton = new MenuButton ("Dodaj przepis");
		showProductButton = new MenuButton ("Pokaz produkty");
		showRecipesButton = new MenuButton("Pokaz przepisy");
		createLabel();
		createButtons();
	}
	
	public Label getLabel()
	{
		return mainLabel;
	}
	
	public Button getAddProductButton()
	{
		return addProductButton;
	}
	
	public Button getAddRecipeButton()
	{
		return addRecipeButton;
	}
	
	public Button getShowProductButton()
	{
		return showProductButton;
	}
	
	public Button getShowRecipesButton()
	{
		return showRecipesButton;
	}
	
	private void createLabel()
	{
		//Tworze glowny napis w oknie apki oraz zmiane po naduszeniu przycisku	
		Font font = new Font(FONT, 20); // przekazuje go do Label aby uzyskac odpowiednia czcionke
		mainLabel.setFont(font);
		mainLabel.setStyle("-fx-font-weight: bold");
		//mainLabel.setTextFill(Color.RED);
		mainLabel.setLayoutX(370);
		mainLabel.setLayoutY(20);
		
	}
	
	private void createButtons() // metoda tworzaca przyciski
	{
		//ustalenie polozenia przyciskow i wymiarow przyciskow
		addProductButton.setLayoutX(50);
		addRecipeButton.setLayoutX(50);
		showProductButton.setLayoutX(50);
		showRecipesButton.setLayoutX(50);
		
		addProductButton.setLayoutY(50);
		addRecipeButton.setLayoutY(120);
		showProductButton.setLayoutY(190);
		showRecipesButton.setLayoutY(260);
		
			
		
		addProductButton.setOnAction(new EventHandler<ActionEvent>(){

			@Override
			public void handle(ActionEvent arg0) {
				AddProduct addProduct = new AddProduct();
			}
			
		});
			
		addRecipeButton.setOnAction(new EventHandler<ActionEvent>(){

			@Override
			public void handle(ActionEvent arg0) {
				AddRecipe addRecipe = new AddRecipe();
				
				
			}
			
		});
		
		showProductButton.setOnAction(new EventHandler<ActionEvent>(){

			@Override
			public void handle(ActionEvent arg0) {
				ShowProduct showProduct = new ShowProduct();
				
			}
			
		});
		
		showRecipesButton.setOnAction(new EventHandler<ActionEvent>(){

			@Override
			public void handle(ActionEvent arg0) {
				ShowRecipe showRecipe = new ShowRecipe();			
			}			
		});		
	}
	

}
