package view;

import java.util.List;

import application.DataBase;
import application.Product;
import application.Recipe;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.event.EventHandler;

public class ShowRecipe {
	
	TableView <Recipe> TableWithRecipes;
	ObservableList<Recipe> listOfRecipes;
	List<Recipe> temporaryList;
	DataBase dataBase = new DataBase();
	
	
	public ShowRecipe()
	{
		createMainTabelWithRecipe();
	}
	
	
	public void createMainTabelWithRecipe()
	{
		//Tworzenie tabeli i listy obserwowalnej
		TableWithRecipes = new TableView<>();
		TableWithRecipes.setLayoutX(350);
		TableWithRecipes.setLayoutY(70);
		temporaryList = dataBase.loadRecipes();
		listOfRecipes = FXCollections.observableArrayList(temporaryList);
		TableWithRecipes.setItems(listOfRecipes);
		createColumnsForTableWithRecipes();
		
		
		//zwalnianie pamieci
		ObservableList<Node> children = ViewManager.mainPane.getChildren();
		if(children.size() > 5) {
		children.remove(children.size()-1);
		}
		ViewManager.mainPane.getChildren().add(TableWithRecipes);
		//System.out.println(ViewManager.mainPane.getChildren().size());
		actionToClick();
	}
	
	public void createColumnsForTableWithRecipes()
	{
		//kolumny
				TableColumn<Recipe, String> nameRecipe = new TableColumn<>("Nazwa przepisu");
				nameRecipe.setMinWidth(150);
				nameRecipe.setCellValueFactory(new PropertyValueFactory<Recipe, String>("name"));
				TableWithRecipes.getColumns().add(nameRecipe);
				
				TableColumn<Recipe, Integer> kcalRecipe = new TableColumn<>("Kalorie");
				kcalRecipe.setMinWidth(150);
				kcalRecipe.setCellValueFactory(new PropertyValueFactory<Recipe, Integer>("kcal"));
				TableWithRecipes.getColumns().add(kcalRecipe);
	}
	
	//otwieranie zawartosci przpeisu 
	public void actionToClick()
	{
		TableWithRecipes.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>(){

			@Override
			public void handle(MouseEvent event) {
				if(event.getClickCount() == 2)
				{
					Recipe recipe = listOfRecipes.get(TableWithRecipes.getSelectionModel().getSelectedIndex());
					IngredientsFromTheRecipe ingredientsFromTheRecipe = new IngredientsFromTheRecipe(recipe);
				}
			}
			
		});
	}
}
