package view;


import java.util.List;

import application.DataBase;
import application.Product;
import application.Recipe;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class IngredientsFromTheRecipe {
	
	Stage stage = new Stage();
	AnchorPane pane = new AnchorPane();
	
	DataBase dataBase = new DataBase();
	TableView<Product> tableWithIngredients;
	ObservableList<Product> listWithIngredients;
	List<Product> temporaryIngredientsList;
	Recipe recipe;
	
	
	public IngredientsFromTheRecipe(Recipe nameOfRecipe)
	{
		this.recipe = nameOfRecipe;
		createStageWithList();
	}
	
	public void createStageWithList()
	{
		final Font FONT = new Font("Arial", 15.0);
		
		Scene scene = new Scene(pane, 300,500);
		stage.setResizable(false);
		stage.setTitle("Skladniki przepisu");
		stage.initOwner(ViewManager.mainStage);
		stage.initModality(Modality.WINDOW_MODAL);
		stage.setScene(scene);
		
		Label labelWithName =  new Label("Nazwa przepisu: " + recipe.getName());
		labelWithName.setLayoutX(60);
		labelWithName.setLayoutY(20);
		labelWithName.setFont(FONT);
		pane.getChildren().add(labelWithName);
		
		tableWithIngredients = new TableView<>();
		tableWithIngredients.setLayoutX(20);
		tableWithIngredients.setLayoutY(50);
		temporaryIngredientsList = dataBase.showListOfProductsForRecipe(recipe);
		listWithIngredients = FXCollections.observableArrayList(temporaryIngredientsList);
		tableWithIngredients.setItems(listWithIngredients);
		loadColumns();
		
		pane.getChildren().add(tableWithIngredients);
		
		stage.show();
	}
	
	public void loadColumns()
	{
		tableWithIngredients.setEditable(true);
		
		TableColumn<Product, String> nameColumn =  new TableColumn<Product, String>("Nazwa produktu");
		nameColumn.setPrefWidth(150);
		nameColumn.setCellValueFactory((new PropertyValueFactory<Product, String>("name")));
		tableWithIngredients.getColumns().add(nameColumn);
		
		TableColumn<Product, Integer> kcalColumn = new TableColumn<Product,Integer>("Ilosc gram");
		kcalColumn.setPrefWidth(100);
		kcalColumn.setCellValueFactory(new PropertyValueFactory<Product, Integer>("gram"));
		tableWithIngredients.getColumns().add(kcalColumn);
		
		//pane.getChildren().add(tableWithIngredients);
	}
	
}
