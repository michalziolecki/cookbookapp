package application;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class Recipe {

	private final SimpleIntegerProperty id;
	private final SimpleStringProperty name;
	private SimpleIntegerProperty kcal; 
	
	public Recipe(int idRecipe, String nameRecipe)
	{
		this.id = new SimpleIntegerProperty( idRecipe);
		this.name = new SimpleStringProperty(nameRecipe);
		this.kcal = new SimpleIntegerProperty(0);
		
	}
	
	public int getId()
	{
		return id.get(); 
	}
	
	public String getName()
	{
		return name.get();
	}
	
	public int getKcal()
	{
		return kcal.get();
	}
	
	public void setKcal(int Calories)
	{
		this.kcal = new SimpleIntegerProperty(Calories);
	}
	@Override
	public String toString()
	{
		return "Recipe [id=" + id.get() + ", name= " + name.get() + "]";
	}
	
}
