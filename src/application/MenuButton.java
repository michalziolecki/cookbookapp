package application;

import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

public class MenuButton extends Button {

	private final String BUTTON_FREE_STYLE = "-fx-background-color: transparent;-fx-background-image: url('/resources/grey_button.png');";
	private final String BUTTON_PRESSED_STYLE = "-fx-background-color: transparent;-fx-background-image: url('/resources/grey_button_pressed.png');";

	public MenuButton(String text) {
		// domyslnie przycisk nie jest wcisniety, dlatego nadajemy mu niewcisniety
		// wyglad
		setStyle(BUTTON_FREE_STYLE);
		setPrefWidth(190);
		setPrefHeight(49);
		setFont(Font.font("Verdana", 15));
		setText(text);
		initializeButtonListeners();
	}

	private void initializeButtonListeners() {
		setOnMousePressed(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				// sprawdzamy, czy wcisniecie myszki bylo wywolane "glownym" przyciskiem, czyli
				// lewym
				if (event.getButton().equals(MouseButton.PRIMARY)) {
					setButtonPressedLayout();
				}
			}
		});

		setOnMouseReleased(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				setButtonReleasedLayout();
			}
		});

		//najechanie na przycisk myszka
		setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				//setEffect(new DropShadow());
				setEffect(new DropShadow(15.0, Color.BLACK));
			}
		});
		//najechanie na przycisk myszka
		setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				setEffect(null);
			}
		});
	}

	private void setButtonPressedLayout() {
		setStyle(BUTTON_PRESSED_STYLE);
		// "wcisnieta" grafika przycisku ma 45 pixeli
		setPrefHeight(45);
		// poniewaz nasz przycisk jest "wcisniety" musimy go przesunac o 4 pixele w dol
		setLayoutY(getLayoutY() + 4);
	}

	private void setButtonReleasedLayout() {
		setStyle(BUTTON_FREE_STYLE);
		setPrefHeight(49);
		setLayoutY(getLayoutY() - 4);
	}

}
