package application;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class Product {
	
	private final SimpleIntegerProperty id;
	private final SimpleStringProperty name;
	private final SimpleIntegerProperty kcal;
	private SimpleIntegerProperty gram;
	
	public Product (int id, String name, int kcal)
	{
		this.id = new SimpleIntegerProperty(id);
		this.name = new SimpleStringProperty(name);
		this.kcal = new SimpleIntegerProperty(kcal);
		this.gram = new SimpleIntegerProperty(0);
	}
	
	public void setGram(int grams)
	{
		this.gram = new SimpleIntegerProperty(grams);
	}
	
	public int getGram()
	{
		return gram.get();
	}
	
	public int getId()
	{
		return id.get();
	}
	
	public String getName()
	{
		return name.get();
	}
	
	public int getKcal()
	{
		return kcal.get();
	}
	
	public String toString()
	{
		return "Product [id=" + id.get() + ", name= " + name.get() + ", kcal= " + kcal.get() + ", gram= "+ gram.get() + "]";
	}
}
