package application;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DataBase {

	// dane do logowania do BD
	final String driver = "com.mysql.jdbc.Driver";
	final String dbPath = "jdbc:mysql://localhost:3306/trening";
	final String LOGIN = "root";
	final String PASS = "admin";

	public DataBase() {

	}

	/**
	 * Metoda pobierajaca wszystkie produkty zapisane w  bazie danych
	 */
	public List<Product> loadProducts() {
		List<Product> existProducts = new ArrayList<>();
		// ViewManager.products.clear();
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			System.out.println("Nie udalo sie zaladowac sterownika bazy danych");
		}
		final String query = "SELECT * FROM produkty";
		try (Connection conn = DriverManager.getConnection(dbPath, LOGIN, PASS);
				Statement statement = conn.createStatement();
				ResultSet resultSet = statement.executeQuery(query)) {
			while (resultSet.next()) {
				// System.out.println("Wchodze do petli");
				int id = resultSet.getInt("idProduktu");
				String name = resultSet.getString("nazwa");
				int kcal = resultSet.getInt("kalorie");
				Product product = new Product(id, name, kcal);
				// System.out.println("Kolerie w obiekcie: "+product.getKcal());
				existProducts.add(product);
				//System.out.println(product);
			}

		} catch (SQLException e) {
			System.out.println("Zle zapytanie przy ladowaniu produktow z bazy" + e.getMessage());
		}
		return existProducts;

	}

	// Ladowanie przepisow z bazy banych
	public List<Recipe> loadRecipes() {
		List<Recipe> existRecipes = new ArrayList<>();
		// ViewManager.products.clear();
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			System.out.println("Nie udalo sie zaladowac sterownika bazy danych");
		}
		final String query = "SELECT * FROM przepisy";
		try (Connection conn = DriverManager.getConnection(dbPath, LOGIN, PASS);
				Statement statement = conn.createStatement();
				ResultSet resultSet = statement.executeQuery(query)) {
			while (resultSet.next()) {
				// System.out.println("Wchodze do petli");
				int id = resultSet.getInt("id");
				String name = resultSet.getString("nazwa");
				Recipe recipe = new Recipe(id, name);
				existRecipes.add(recipe);
				//System.out.println(recipe);
			}

		} catch (SQLException e) {
			System.out.println("Zle zapytanie przy ladowaniu przepisow z bazy" + e.getMessage());
		}
		//laduje tutaj ilosc kalori dla kazdej potrawy - przy pomocy kolejnej metody tej klasy 
		for(int i = 0; i < existRecipes.size(); i++) {
			existRecipes.get(i).setKcal(loadKcalForRecipe(existRecipes.get(i)));
		}
		
		return existRecipes;
	}

	// ------- zapisanie produktow do bazy danych w tabeli produkty
	public void saveProductInDataBase(String name, int kcal) {
		// ladowanie sterownika
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			System.out.println("Nie załadowano sterownika bazy danych");
		}
		// logowanie
		try {
			// loguje sie do bazy
			Connection conn = DriverManager.getConnection(dbPath, LOGIN, PASS);
			Statement statement = conn.createStatement();
			final String query = "INSERT INTO produkty (nazwa, kalorie) VALUES (' " + name + " ', " + kcal + ");";
			// wykonuje zapytanie i zwrot w postaci zmiany rekordow zapisuje do inta
			int rowsAffected = statement.executeUpdate(query);
			System.out.println("Wykonano zmian rekordow: " + rowsAffected);
		} catch (SQLException e) {
			System.out.println("Problem z dodaniem rekordu produktu" + e.getMessage());
		}
		// loadProducts();
	}

	// zapisanie nazwy przepisu do tabeli + nadaje baza automatycznie id
	public void saveRecipeInDataBase(String name) {
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			System.out.println("Nie zaladowano sterownika: " + e.getMessage());
		}

		try (Connection conn = DriverManager.getConnection(dbPath, LOGIN, PASS);
				Statement statement = conn.createStatement();) {
			final String query = "INSERT INTO przepisy (nazwa) VALUES ('" + name + "');";
			int rowsAffected = statement.executeUpdate(query);
			System.out.println("Wprowadzono rekordow: " + rowsAffected);
			// dodac ladowanie produktow w liscie tu?
		} catch (SQLException e) {
			System.out.println("Blad w zapytaniu do bazy (zapis przepisu): " + e.getMessage());
		}
	}

	// zczytuje przepis z bazy i podaje do AddRecipe celem stworzeni relacji z
	// produktami (tu juz mam id przepisu )
	public Recipe readRecipeFromDataBase(String name) {
		try {
			Class.forName(driver);

		} catch (ClassNotFoundException e) {
			System.out.println("Nie zaladowano sterownika: " + e.getMessage());
		}
		Recipe readRecipeToreturn = null;
		final String query = "SELECT * FROM przepisy WHERE nazwa = '" + name + "';";

		try (Connection conn = DriverManager.getConnection(dbPath, LOGIN, PASS);
				Statement statement = conn.createStatement();
				ResultSet resultSet = statement.executeQuery(query);) {
			while (resultSet.next()) {
				int id = resultSet.getInt("id");
				String nazwa = resultSet.getString("nazwa");
				readRecipeToreturn = new Recipe(id, nazwa);
				
			}

		} catch (SQLException e) {
			System.out.println("Problem z pobraniem przepisu z bazy: " + e.getMessage());
		}

		return readRecipeToreturn;
	}

	public void saveProductsForRecipe(Recipe recipe, List<Product> listOfPRoductsinRecipes) {
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			System.out.println("Nie zaladowano sterownika: " + e.getMessage());
		}

		try (Connection conn = DriverManager.getConnection(dbPath, LOGIN, PASS);
				Statement statement = conn.createStatement();) {
			for (Product product : listOfPRoductsinRecipes) {
				final String query = "INSERT INTO produkty_przepisy (produkty_id, przepisy_id, gram) VALUES ('"
						+ product.getId() + "','" + recipe.getId() + "','" + product.getGram() + "');";
				int rowAffected = statement.executeUpdate(query);
				System.out.println("Wprowadzono do produkty_przepsy nowych: " + rowAffected + " rekordow");
			}

		} catch (SQLException e) {
			System.out.println("Problem z zapisaniem relacje produkt - przepis do bazy: " + e.getMessage());
		}

	}

	public int loadKcalForRecipe(Recipe recipe) {
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			System.out.println("Nie zaladowano sterownika: " + e.getMessage());
		}

		final String query = "select produkty.kalorie, produkty_przepisy.gram from produkty "
				+ "join produkty_przepisy on produkty.idProduktu = produkty_przepisy.produkty_id "
				+ "join przepisy on produkty_przepisy.przepisy_id = przepisy.id "
				+ "where przepisy.id ='" + recipe.getId() + "';";
		List<Integer> kcalList = new ArrayList<>();
		try (Connection conn = DriverManager.getConnection(dbPath, LOGIN, PASS);
				Statement statement = conn.createStatement();
				ResultSet resultSet = statement.executeQuery(query)) {
			while (resultSet.next()) {
				int kcal = resultSet.getInt("kalorie");
				int gram = resultSet.getInt("gram");
				// sum kcal for one product
				int sumOfkcal = ((kcal*gram)/100);
				kcalList.add(sumOfkcal);
			}

		} catch (SQLException e) {
			System.out.println("Zle dane?" + e.getLocalizedMessage() + e.getMessage());
		}
		int resultKcal = 0;
		for(int i = 0; i < kcalList.size(); i++) {
			resultKcal += kcalList.get(i);
		}
		return resultKcal;
	}

	public boolean checkStatusForNameOfProduct(String nameOfProduct)
	{
		final String query = "SELECT * FROM produkty WHERE nazwa = '" + nameOfProduct + "';";
		boolean test = false;
		try {
			Class.forName(driver);
		}catch(ClassNotFoundException e)
		{
			System.out.println("Problem ze sterownikiem" + e.getMessage());
		}
		
		try( Connection connection = DriverManager.getConnection(dbPath, LOGIN, PASS);
				Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery(query);
				){
			test = resultSet.first();
			
			//System.out.println("Test w bazie: " + test);
		}catch(SQLException e)
		{
			System.out.println("Blad ze sprawdzeniem obecnosci produktu przez zapytanie " + e.getMessage());
		}
		
		return test;
	}
	
	public boolean checkStatusForNameOfRecipe(String nameOfRecipe)
	{
		String query = "SELECT * FROM przepisy WHERE nazwa = '" + nameOfRecipe + "';";
		boolean test = false;
		try
		{
			Class.forName(driver);
		}catch(ClassNotFoundException e)
		{
			System.out.println("Problem ze sterownikiem przy sprawdzneiu przpeisu" + e.getMessage());
		}
		try(Connection connection = DriverManager.getConnection(dbPath, LOGIN, PASS);
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(query);
				){
			test = resultSet.first();
			
		}catch(SQLException e)
		{
			System.out.println("Blad w wykonaniu zapytania do bazy przy sprawdzeniu nazwy produktu" + e.getMessage() );
		}	
		return test;
	}
	
	public List<Product> showListOfProductsForRecipe(Recipe recipe)
	{
		List<Product> listOfProduct = new ArrayList();
		Product product;
		String query = "SELECT produkty.idProduktu, produkty.kalorie, produkty.nazwa, produkty_przepisy.gram FROM produkty "
				+ "JOIN produkty_przepisy ON produkty_przepisy.produkty_id = produkty.idProduktu "
				+ "JOIN przepisy ON produkty_przepisy.przepisy_id = przepisy.id WHERE przepisy.id ="
				+ recipe.getId()+";";
		try {
			Class.forName(driver);
		}catch(ClassNotFoundException e)
		{
			System.out.println("Problem w polaczeniu z baza przy zwracania produktow do przepisu" + e.getMessage());
		}
		
		try(	Connection connection = DriverManager.getConnection(dbPath, LOGIN, PASS);
				Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery(query);
				){
				while(resultSet.next()) {
					int id = resultSet.getInt("idProduktu");
					int kcal = resultSet.getInt("kalorie");
					String name = resultSet.getString("nazwa");
					int gram = resultSet.getInt("gram");
					product = new Product(id, name, kcal);
					product.setGram(gram);
					listOfProduct.add(product);
				}
			
		}catch(SQLException e)
		{
			System.out.println("Blad w bloku zapytania SQL przy zwracaniu listy produktow do przepisu" + e.getMessage());
		}
		
		return listOfProduct;
	}
	
}
